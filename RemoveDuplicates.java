import java.util.Arrays;
import java.util.Scanner;

class Solution {
    public int maximumProduct(int[] nums) {
        if (nums == null || nums.length < 3) {
            System.out.println("Insufficient elements to find maximum product.");
            return -1;
        }

        Arrays.sort(nums);

        int n = nums.length;
        int product1 = nums[n - 1] * nums[n - 2] * nums[n - 3];
        int product2 = nums[0] * nums[1] * nums[n - 1];
        
        return Math.max(product1, product2);
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the length of the array: ");
        int n = scanner.nextInt();

        int[] nums = new int[n];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }

        int result = solution.maximumProduct(nums);
        System.out.println("Maximum Product: " + result);
    }
}
